{
  description = "Convenience wrapper for Watch";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }:
  
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {
    packages.${system}.default = pkgs.writeShellScriptBin "observe" "watch -n 1 -d \"$@\"";
  };
}