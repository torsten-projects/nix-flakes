{
  description = "Kubernetes LDAP authentication provider";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.${system};
  in
  {

    packages.${system}.default = pkgs.buildGoModule rec {
      pname = "k8s-ldap-auth";
      version = "4.0.0";

      src = pkgs.fetchFromGitHub {
        owner = "vbouchaud";
        repo = "k8s-ldap-auth";
        rev = "v${version}";
        hash = "sha256-if73r1mgUS1eldlxtUwxOcT3EyRHJHAJMvmwS8vE040=";
      };

      vendorHash = "sha256-QqimCdcpz2i/c/S0+mo2qB/maKlRFrXgDdRRLOAetZA=";

      meta = with pkgs.lib; {
        homepage = "https://github.com/vbouchaud/k8s-ldap-auth";
        description = "LDAP authentication for Kubernetes";
        license = licenses.mpl20;
        platforms = platforms.linux;
      };
    };

  };
}
